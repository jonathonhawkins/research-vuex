// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
// third party
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import App from './App';
import router from './router';
import { store } from './store/index';

// stores
// import authStore from './modules/authentication/AuthStore';


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App,
  },
  store,
  template: '<App/>',
});
