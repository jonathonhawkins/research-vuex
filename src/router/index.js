import Vue from 'vue';
import Router from 'vue-router';
import BootstrapVue from 'bootstrap-vue';

Vue.use(Router);
Vue.use(BootstrapVue);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: () => import('@/pages/login'),
      props: {
        default: true,
        navigation: true,
      },
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/pages/home'),
      props: {
        default: true,
        navigation: true,
      },
    },
    {
      path: '/success',
      name: 'success',
      component: () => import('@/pages/success'),
      props: {
        default: true,
        navigation: true,
      },
    },
  ],
});
