import * as types from './types';

const getters = {
  auth: state => state.auth,
};

// actions
const actions = {
  updateAuth({ commit }, auth) {
    setTimeout(() => {
      commit(types.UPDATE_AUTH, auth);
    }, 1000);
  },
};

// mutations
const mutations = {
  [types.UPDATE_AUTH](state, auth) {
    state.auth = auth;
  },
};

const module = {
  state: {
    auth: {
      username: 'Bruce',
      password: 'Batman',
    },
  },
  getters,
  actions,
  mutations,
};

export default module;
